* Project funds

All units in USD.

** Grants

| Grant        | Start       | Expiry      | Value  |
|--------------+-------------+-------------+--------|
| AI for Earth | August 2019 | August 2022 | 10,000 |

** Funds

| Month   | Description         | Credit |  Debit |             Balance |
|---------+---------------------+--------+--------+---------------------|
| 2020/01 | Opening balance     |        |        |                 0.0 |
| 2020/01 | Azure hosting       |        |  90.00 |               -90.0 |
| 2020/01 | AI for Earth grant  |  90.00 |        |                 0.0 |
| 2020/01 | AWS Hosting         |        |   5.49 |               -5.49 |
| 2020/02 | Azure hosting       |        | 132.00 |             -137.49 |
| 2020/02 | AI for Earth grant  | 132.00 |        |               -5.49 |
| 2020/02 | AWS Hosting         |        |   5.82 |              -11.31 |
| 2020/03 | Azure hosting       |        |  80.00 |              -91.31 |
| 2020/03 | AI for Earth grant  |  80.00 |        |              -11.31 |
| 2020/03 | AWS Hosting         |        |   5.67 |              -16.98 |
| 2020/04 | Azure hosting       |        |  89.00 |             -105.98 |
| 2020/04 | AI for Earth grant  |  89.00 |        |              -16.98 |
| 2020/04 | AWS Hosting         |        |   5.55 |              -22.53 |
| 2020/05 | AWS Hosting         |        |   5.66 |              -28.19 |
| 2020/05 | Donation            |  12.19 |        |               -16.0 |
| 2020/05 | Azure hosting       |        | 103.00 |              -119.0 |
| 2020/05 | AI for Earth grant  | 103.00 |        |               -16.0 |
| 2020/05 | Domain registration |        |  13.20 |               -29.2 |
| 2020/06 | AWS Hosting         |        |   6.81 |              -36.01 |
| 2020/06 | Azure hosting       |        |  99.00 |             -135.01 |
| 2020/06 | AI for Earth grant  |  99.00 |        |              -36.01 |
| 2020/07 | AWS Hosting         |        |   8.80 |              -44.81 |
| 2020/07 | Azure hosting       |        | 118.00 |             -162.81 |
| 2020/07 | AI for Earth grant  | 118.00 |        |              -44.81 |
| 2020/08 | AWS Hosting         |        |   9.34 |              -54.15 |
| 2020/08 | Azure hosting       |        | 155.00 |             -209.15 |
| 2020/08 | AI for Earth grant  | 155.00 |        |              -54.15 |
| 2020/09 | Azure hosting       |        | 157.00 |             -211.15 |
| 2020/09 | AI for Earth grant  | 157.00 |        |              -54.15 |
| 2020/09 | AWS hosting         |        |   6.55 |               -60.7 |
| 2020/09 | Donation            |  22.80 |        |               -37.9 |
| 2020/09 | Donation            |  35.80 |        |                -2.1 |
| 2020/10 | AWS hosting         |        |   6.81 |               -8.91 |
| 2020/10 | Azure hosting       |        | 123.00 |             -131.91 |
| 2020/10 | AI for Earth grant  | 123.00 |        |               -8.91 |
| 2020/11 | AWS hosting         |        |   6.62 | -15.530000000000001 |
| 2020/11 | Azure hosting       |        | 186.00 |             -201.53 |
| 2020/11 | AI for Earth grant  | 186.00 |        | -15.530000000000001 |
| 2020/11 | Donation            |  57.28 |        |               41.75 |
| 2020/12 | Azure hosting       |        | 193.00 |             -151.25 |
| 2020/12 | AI for Earth grant  | 193.00 |        |               41.75 |
| 2020/12 | AWS hosting         |        |   6.83 |               34.92 |
| 2021/01 | Azure hosting       |        | 186.00 |             -151.08 |
| 2021/01 | AI for Earth grant  | 186.00 |        |               34.92 |
| 2021/01 | AWS hosting         |        |   6.95 |               27.97 |
| 2021/02 | Azure hosting       |        | 200.00 |             -172.03 |
| 2021/02 | AI for Earth grant  | 200.00 |        |               27.97 |
| 2021/02 | AWS hosting         |        |   6.95 |               21.02 |
| 2021/03 | Azure hosting       |        | 168.00 |             -146.98 |
| 2021/03 | AI for Earth grant  | 168.00 |        |               21.02 |
| 2021/03 | AWS hosting         |        |   7.71 |               13.31 |
| 2021/04 | Azure hosting       |        | 147.00 |             -133.69 |
| 2021/04 | AI for Earth grant  | 147.00 |        |               13.31 |
| 2021/04 | AWS hosting         |        |   6.81 |                 6.5 |
| 2021/05 | Azure hosting       |        | 205.00 |              -198.5 |
| 2021/05 | AI for Earth grant  | 205.00 |        |                 6.5 |
| 2021/05 | AWS hosting         |        |   7.09 |               -0.59 |
| 2021/05 | Domain renewal      |        |  13.20 | -13.790000000000001 |
| 2021/06 | Azure hosting       |        | 128.00 |             -141.79 |
| 2021/06 | AI for Earth grant  | 128.00 |        | -13.790000000000001 |
| 2021/06 | AWS hosting         |        |   7.61 | -21.400000000000002 |
| 2021/07 | Azure hosting       |        | 231.00 |              -252.4 |
| 2021/07 | AI for Earth grant  | 231.00 |        | -21.400000000000002 |
| 2021/07 | AWS hosting         |        |   6.81 |              -28.21 |
| 2021/08 | Azure hosting       |        | 172.00 |             -200.21 |
| 2021/08 | AI for Earth grant  | 172.00 |        |              -28.21 |
| 2021/08 | AWS hosting         |        |   7.79 |               -36.0 |
| 2021/09 | Azure hosting       |        | 177.00 |              -213.0 |
| 2021/09 | AI for Earth grant  | 177.00 |        |               -36.0 |
| 2021/09 | AWS hosting         |        |   7.57 |              -43.57 |
| 2021/10 | Azure hosting       |        | 236.00 |             -279.57 |
| 2021/10 | AI for Earth grant  | 236.00 |        |              -43.57 |
| 2021/10 | AWS hosting         |        |   7.82 |              -51.39 |
| 2021/11 | Azure hosting       |        | 184.00 | -235.39000000000001 |
| 2021/11 | AI for Earth grant  | 184.00 |        |              -51.39 |
| 2021/11 | AWS hosting         |        |   6.58 |              -57.97 |
|---------+---------------------+--------+--------+---------------------|
|         | *Total*             |        |        |              -57.97 |
#+TBLFM: $5='(if (string= @-1 "Balance") 0.00 (* 0.01 (round (* 100 (- (+ (string-to-number @-1) (string-to-number $3))  (string-to-number $4))))))
