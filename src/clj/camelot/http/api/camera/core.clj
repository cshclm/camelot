(ns camelot.http.api.camera.core
  (:require
   [camelot.http.api.camera.spec :as spec]
   [camelot.http.api.camera.resources :as resources]
   [camelot.http.api.spec.core :as api-core]
   [compojure.api.sweet :refer [context DELETE GET POST PATCH]]
   [clojure.spec.alpha :as s]))

(def routes
  (context "/cameras" {state :state}
    :tags ["cameras"]

    (GET "/:id" [id]
      :summary "Retrieve a camera with the given ID"
      :return (s/merge
               ::api-core/json-api-without-data
               (s/keys :req-un [::spec/data]))
      (resources/get-with-id state id))

    (GET "/" []
      :summary "Retrieve all cameras"
      :return (s/merge
               ::api-core/json-api-without-data
               (s/keys :req-un [:camelot.http.api.camera.get-all/data]))
      (resources/get-all state))

    (PATCH "/:id" [id]
      :summary "Update a camera"
      :body [data (s/keys :req-un [:camelot.http.api.camera.patch/data])]
      :return (s/merge
               ::api-core/json-api-without-data
               (s/keys :req-un [::spec/data]))
      (resources/patch state id data))

    (POST "/" []
      :summary "Create a new camera"
      :body [data (s/keys :req-un [:camelot.http.api.camera.post/data])]
      :return (s/merge
               ::api-core/json-api-without-data
               (s/keys :req-un [::spec/data]))
      (resources/post state data))

    (DELETE "/:id" [id]
      :summary "Delete a camera"
      (resources/delete state id))))
