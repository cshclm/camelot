(ns camelot.http.api.camera.resources
  (:require
   [camelot.http.api.camera.spec :as spec]
   [camelot.model.camera :as camera]
   [cats.core :as m]
   [cats.monad.either :as either]
   [clojure.edn :as edn]
   [ring.util.http-response :as hr]
   [camelot.http.api.util :as api-util]))

(def resource-type :camera)

(def resource-base-uri "/api/v1/cameras")

(defn patch [state id data]
  (let [mr (m/->>=
           (either/right data)
           (api-util/transform-request resource-type :camelot.http.api.camera.patch/attributes id)
           (camera/patch! state (edn/read-string id))
           (api-util/mtransform-response resource-type ::spec/attributes))]
    (->> mr
         (m/fmap hr/ok)
         (api-util/to-response))))

(defn post [state data]
  (let [mr (m/->>=
            (either/right data)
            (api-util/transform-request resource-type :camelot.http.api.camera.post/attributes)
            (camera/post! state)
            (api-util/mtransform-response resource-type ::spec/attributes))]
    (->> mr
         (m/fmap #(api-util/created resource-base-uri %))
         (api-util/to-response))))

(defn get-with-id [state id]
  (let [mr (m/->>= (either/right id)
                   (api-util/transform-id)
                   (camera/get-single state)
                   (api-util/mtransform-response resource-type ::spec/attributes))]
    (->> mr
         (m/fmap hr/ok)
         (api-util/to-response))))

(defn get-all [state]
  (let [mr (m/->>= (either/right (camera/get-all state))
                   (api-util/mtransform-response resource-type ::spec/attributes))]
    (->> mr
         (m/fmap hr/ok)
         (api-util/to-response))))

(defn delete [state id]
  (let [mr (camera/mdelete! state (edn/read-string id))]
    (->> mr
         (m/fmap (constantly (hr/no-content)))
         (api-util/to-response))))
