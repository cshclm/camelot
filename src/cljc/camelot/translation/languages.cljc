(ns camelot.translation.languages
  (:require
   [camelot.translation.en :refer [t-en]]
   [camelot.translation.fr :refer [t-fr]]
   [camelot.translation.ne :refer [t-ne]]
   [camelot.translation.ru :refer [t-ru]]))

(def languages
  {:fr {:native-display-name "Français"
        :dictionary t-fr}
   :en {:native-display-name "English"
        :dictionary t-en}
   :ne {:native-display-name "नेपाली"
        :dictionary t-ne}
   :ru {:native-display-name "Русский"
        :dictionary t-ru}})

(def dictionaries
  (into {} (map (juxt key (comp :dictionary val)) languages)))
