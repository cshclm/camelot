(ns camelot.http.api.trap-station.spec
  (:require
   [camelot.spec.model.trap-station :as model-spec]
   [camelot.http.api.spec.core :as api-core]
   [clojure.spec.alpha :as s]))

(s/def ::id ::model-spec/trap-station-id)
(s/def ::surveySiteId ::model-spec/survey-site-id)
(s/def ::name ::model-spec/trap-station-name)
(s/def ::created nat-int?)
(s/def ::updated nat-int?)
(s/def ::longitude ::model-spec/trap-station-longitude)
(s/def ::latitude ::model-spec/trap-station-latitude)
(s/def ::notes ::model-spec/trap-station-notes)

(s/def ::attributes
  (s/keys :req-un [::id
                   ::name
                   ::surveySiteId
                   ::created
                   ::updated
                   ::latitude
                   ::longitude]
          :opt-un [::notes]))

(s/def ::data
  (s/keys :req-un [::api-core/id
                   ::api-core/type
                   ::attributes]))

(s/def :camelot.http.api.trap-station.patch/attributes
  (s/keys :opt-un [::name
                   ::surveySiteId
                   ::latitude
                   ::longitude
                   ::notes]))

(s/def :camelot.http.api.trap-station.patch/data
  (s/keys :req-un [::api-core/id
                   ::api-core/type
                   :camelot.http.api.trap-station.patch/attributes]))

(s/def :camelot.http.api.trap-station.post/attributes
  (s/keys :req-un [::name
                   ::surveySiteId
                   ::latitude
                   ::longitude]
          :opt-un [::notes]))

(s/def :camelot.http.api.trap-station.post/data
  (s/keys :req-un [::api-core/type
                   :camelot.http.api.trap-station.post/attributes]))

(s/def :camelot.http.api.trap-station.get-all/data
  (s/coll-of ::data))
