(ns camelot.http.api.trap-station.resources
  (:require
   [camelot.model.survey :as survey]
   [camelot.model.survey-site :as survey-site]
   [camelot.http.api.trap-station.spec :as spec]
   [camelot.model.trap-station :as trap-station]
   [cats.core :as m]
   [cats.monad.either :as either]
   [clojure.edn :as edn]
   [ring.util.http-response :as hr]
   [camelot.http.api.util :as api-util]))

(def resource-type :trap-station)
(def resource-base-uri "/api/v1/trap-stations")

(defn- hoist
  [v f lv]
  (if v
    (either/right (f v))
    (either/left lv)))

(defn- map-not-found
  [f v]
  (hoist v f {:error/type :error.type/not-found}))

(defn- validate-patch
  [state patch]
  (if-let [ssid (:survey-site-id patch)]
    (hoist (survey-site/get-specific state ssid)
           (constantly patch)
           {:error/type :error.type/bad-request})
    (either/right patch)))

(defn patch [state id data]
  (let [mr (m/->>=
           (either/right data)
           (api-util/transform-request resource-type :camelot.http.api.trap-station.patch/attributes id)
           (validate-patch state)
           (trap-station/patch! state (edn/read-string id))
           (api-util/mtransform-response resource-type ::spec/attributes))]
    (->> mr
         (m/fmap hr/ok)
         (api-util/to-response))))

(defn post [state data]
  (let [mr (m/->>=
            (either/right data)
            (api-util/transform-request resource-type :camelot.http.api.trap-station.post/attributes)
            (trap-station/post! state)
            (api-util/mtransform-response resource-type ::spec/attributes))]
    (->> mr
         (m/fmap #(api-util/created resource-base-uri %))
         (api-util/to-response))))

(defn get-with-id [state id]
  (let [mr (m/->>= (either/right id)
                   (api-util/transform-id)
                   (trap-station/get-single state)
                   (api-util/mtransform-response resource-type ::spec/attributes))]
    (->> mr
         (m/fmap hr/ok)
         (api-util/to-response))))

(defn- get-all*
  [state survey-id]
  (either/right (trap-station/get-all state survey-id)))

(defn get-all [state survey-id]
  (let [mr (m/->>= (survey/get-single state survey-id)
                   (map-not-found :survey-id)
                   (get-all* state)
                   (api-util/mtransform-response resource-type ::spec/attributes))]
    (->> mr
         (m/fmap hr/ok)
         (api-util/to-response))))

(defn delete [state id]
  (let [mr (trap-station/mdelete! state (edn/read-string id))]
    (->> mr
         (m/fmap (constantly (hr/no-content)))
         (api-util/to-response))))
